# Crawler
Extract all url structure from a web site

## Requirements
- node v8.11.3 (prior versions may work, but haven't been tested)

## TL;DR;
```
npx yarn@1.7.0 # or `yarn install` if it's already installed
node cli https://wetaca.com --progress
```

## Usage
The target must be a complete url, like https://wetaca.com.

When targeting big sites where the crawling can take a while, the `--progress`
flag can be used to keep track of the process, and make the wait more bearable.
**Please note** that the progress will fluctuate (showing decreasing numbers even)
as new links are discovered during the process, and serves only as a rough
estimation of the completeness of the site map.

See `node cli --help` for usage details.

## How it works
Internally it visits the provided url and builds a graph of every link within
the same domain (matching the provided url). Then visits each one of those links
in turn in order to find more links in other pages, until the graph is complete
(a point where any discovered link has already been seen before in another page).

The graph is then displayed back on the screen as a tree of urls in a depth-first
fashion.

See DEVELOPERS.md for more implementation details.
