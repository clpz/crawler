/* eslint-env jest */

const {tree, depthFirst} = require('./tree')
const {vertices} = require('./graph')

test('tree of a list of urls', () => {
  const graph = {
    'http://foo.bar': [
      'http://foo.bar/baz',
      'http://foo.bar/foo/bar'
    ],
    'http://foo.bar/foo/bar': [
      'http://foo.bar/baz/quux'
    ],
    'http://foo.bar/baz': [],
    'http://foo.bar/baz/quux': ['http://foo.bar']
  }

  expect(tree(vertices(graph), 'http://foo.bar')).toEqual({
    'http://foo.bar': [
      {'/foo': [
        {'/bar': []}
      ]},
      {'/baz': [
        {'/quux': []}
      ]}
    ]
  })
})

test('depth first heuristic tree exploration', () => {
  const tree = {
    'http://foo.bar': [
      {'/foo': [
        {'/bar': []}
      ]},
      {'/baz': [
        {'/quux': []}
      ]}
    ]
  }
  const expectedCalls = [
    {node: 'http://foo.bar', depth: 0},
    {node: '/foo', depth: 1},
    {node: '/bar', depth: 2},
    {node: '/baz', depth: 1},
    {node: '/quux', depth: 2}
  ]
  const visitor = jest.fn()

  depthFirst(visitor, tree)

  expect(visitor.mock.calls.length).toEqual(expectedCalls.length)

  expectedCalls.forEach((expectedCall, callNumber) =>
    expect(visitor.mock.calls[callNumber][0]).toEqual(expectedCall)
  )
})
