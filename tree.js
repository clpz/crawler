// @flow
const {uniq} = require('./lib')
/*::
opaque type Tree = {
  [string]: Array<Tree>
}
*/
const root = (tree/*: Tree */)/*: string */=> Object.keys(tree)[0]
const branches = tree => tree[root(tree)]

const depthFirst = (
  visitor/*: ({node: string, depth: number}) => any */,
  tree/*: Tree */
) => {
  // uses shift as pop and unshift as push
  // this allows to push multiple elements at once (without needing to reverse)
  const stack = [
    {subtree: tree, depth: 0}
  ]
  while (stack.length !== 0) {
    const {subtree, depth} = stack.shift()

    visitor({node: root(subtree), depth})

    stack.unshift(...branches(subtree).map(branch => ({
      subtree: branch,
      depth: depth + 1
    })))
  }
}

const tree = (urls/*: Array<string> */, prefix/*: string */)/*: Tree */ => {
  const tree = {
    [prefix]: []
  }
  const queue = [
    {path: prefix, branch: tree[prefix]}
  ]
  while (queue.length !== 0) {
    const {path, branch} = queue.shift()
    const children = uniq(urls
      .filter(url =>
        url.startsWith(path) && url.length > path.length
      )
      .map(url =>
        `${path}/${url.replace(`${path}/`, '').split('/')[0]}`
      ))
      .map(url => ({
        path: url,
        branch: []
      }))
    branch.push(...children.map(child => ({
      [child.path.replace(path, '')]: child.branch
    })))
    queue.push(...children)
  }
  return tree
}

module.exports = {tree, depthFirst}
