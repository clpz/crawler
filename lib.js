// @flow
const {parse, format} = require('url')

// normalize: strips just the relevant parts of any url or path
// http://foo.bar/foo/bar?baz#quxx => http://foo.bar/foo/bar
// /some/path/ => some/path
const normalize = (url/*: string */)/*: string */=> {
  const {protocol, auth, host, pathname} = parse(url)
  return format({protocol, auth, host, pathname}).replace(/^\/|\/$/g, '')
}

// uniq: removes duplicates
const uniq = (array/*: Array<*> */)/*: Array<*> */=>
  [...new Set(array)]

module.exports = {
  normalize,
  uniq
}
