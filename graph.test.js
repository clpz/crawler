/* eslint-env jest */

const {explore, edges, vertices, graph, addVertex} = require('./graph')

const createTestGraph = () => {
  const testGraph = graph()
  addVertex(testGraph, 'v1', ['v2', 'v3'])
  addVertex(testGraph, 'v2', ['v3', 'v4'])
  addVertex(testGraph, 'v3', [])
  addVertex(testGraph, 'v4', ['v1'])
  return testGraph
}

test('vertices', () =>
  expect(vertices(createTestGraph())).toEqual([
    'v1',
    'v2',
    'v3',
    'v4'
  ])
)

test('edges', () =>
  expect(edges(createTestGraph(), 'v1')).toEqual(['v2', 'v3'])
)

// TODO: this is testing the actual implementation of the underlying data
// structure. It should use other methods (such as calling includes) to test the
// expected behavior. However it's enough to ensure it works as expected.
test('explore builds the sitemap graph', async () => {
  expect.assertions(1)
  const sitemap = {
    'http://foo.bar/': [
      '/foobar',
      'http://foo.bar/baz',
      'http://foo.bar/quux'
    ],
    '/foobar': [
      'http://foo.bar/baz',
      'http://foo.bar/quux'
    ],
    'http://foo.bar/baz': [
      '/foobar'
    ],
    'http://foo.bar/quux': [
      'http://foo.bar/baz'
    ]
  }
  const visitor = url => Promise.resolve(sitemap[url])

  const exploredGraph = await explore(visitor, 'http://foo.bar/')

  expect(exploredGraph).toEqual(sitemap)
})
