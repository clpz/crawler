// @flow

// This barebones implementation of a graph uses a map where keys represent
// vertices and values are either undefined or an array of other vertices (the
// opposite ends of edges from that starting point). If a vertex has undefined
// edges, it's never been visited (the graph is not done yet).
/*::
export opaque type Graph = {[string]: ?Array<string>}
*/

const graph = ()/*: Graph */=> ({})

const addVertex = (
  graph/*: Graph */,
  newVertex/*: string */,
  edges/*: ?Array<string> */
) => {
  graph[newVertex] = edges
  if (Array.isArray(edges)) {
    edges
      .filter(vertex => !vertices(graph).includes(vertex))
      .forEach(vertex => (graph[vertex] = undefined))
  }
}

const vertices = (graph/*: Graph */)/*: Array<string> */=>
  Object.keys(graph)

const edges = (graph/*: Graph */, vertex/*: string */)/*: ?Array<string> */=>
  graph[vertex]

// isVisited: a vertex is visited if contains an array of other vertices, even if it's empty
const isVisited = (graph/*: Graph */, vertex) => Array.isArray(edges(graph, vertex))
// isFullyVisited: a graph is "done" if all of its vertices have been visited
const isFullyVisited = (graph/*: Graph */)/*: bool */=>
  vertices(graph).every(vertex =>
    isVisited(graph, vertex)
  )
const completePercent = graph => {
  const total = vertices(graph)
  const visited = total.filter(vertex => isVisited(graph, vertex))
  return visited.length / total.length
}

const includes = (graph/*: Graph */, vertex) =>
  vertices(graph).includes(vertex)

const extend = (destination, source) =>
  Object.assign(destination, source)

const explore = async (
  visitor/*: string => Promise<Array<string>> */,
  target/*: string */,
  updateProgress/*: number => void */ = () => undefined
)/*: Promise<Graph> */=> {
  const exploredGraph = graph()
  addVertex(exploredGraph, target)
  while (!isFullyVisited(exploredGraph)) {
    for (let vertex of vertices(exploredGraph)) {
      if (!isVisited(exploredGraph, vertex)) {
        const newEdges = await visitor(vertex).catch(_ => [])
        addVertex(exploredGraph, vertex, newEdges)
        const newlyDiscoveredGraph = graph()
        newEdges
          .filter(otherVertex => !includes(exploredGraph, otherVertex))
          .forEach(otherVertex =>
            addVertex(newlyDiscoveredGraph, otherVertex)
          )
        extend(exploredGraph, newlyDiscoveredGraph)
        updateProgress(completePercent(exploredGraph))
      }
    }
  }
  return exploredGraph
}

module.exports = {
  graph,
  explore,
  addVertex,
  edges,
  vertices
}
