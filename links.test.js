/* eslint-env jest */

const {$, isRelative} = require('./links')

test('detects wheter or not a link isRelative', () =>
  expect(isRelative('/foo')).toBe(true)
)

test('$ applies a selector to an html string', () => {
  const html = `<html><body><a href="/foo"/><a href="bar"/></body></html>`
  const links = $('a[href]', html)
  expect(links).toHaveLength(2)
  expect(links.map(link => link.getAttribute('href')))
    .toEqual([
      '/foo',
      'bar'
    ])
})
