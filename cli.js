// @flow
const {URL} = require('url')
const ProgressBar = require('progress')

const {vertices, explore} = require('./graph')
const {tree, depthFirst} = require('./tree')
const {fetch} = require('./links')

const consoleLogger = ({depth, node}) =>
  console.log(`${Array.from({length: depth}).map(_ => '| ').join('')}|->${node}`)

const main = async (targetUrl, showProgress) => {
  let updateProgress = () => undefined
  if (showProgress) {
    const progressBar = new ProgressBar(':bar :percent', {total: 100})
    updateProgress = percent => progressBar.update(percent)
  }
  const linkGraph = await explore(fetch(targetUrl), targetUrl, updateProgress)
  const linkTree = tree(vertices(linkGraph), targetUrl)
  depthFirst(consoleLogger, linkTree)
}

const cli = () =>
  require('yargs')
    // https://github.com/yargs/yargs/blob/8edbce/docs/advanced.md#default-commands
    .command('$0 <url>', 'print all links found at some url',
      yargs =>
        yargs
          .positional('url', {
            description: 'full url to start crawling (including protocol: http(s)://)',
            type: 'string'
          })
          .example('$0 https://wetaca.com')
          .coerce('url', url => {
            // throw if the provided url can't be parsed
            void new URL(url) // attempt parsing (void here means intentional)
            return url // but return its string value as is
          }),
      ({url, progress}) =>
        main(url, progress)
    )
    .option('progress', {
      description: 'display a progressbar while crawling',
      boolean: true
    })
    .fail((msg, err, yargs) => {
      console.error(msg)
      console.error('usage:', yargs.help())
      process.exit(-1)
    })
    .argv

if (!module.parent) {
  cli()
}
