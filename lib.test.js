/* eslint-env jest */
const jsc = require('jsverify')
const {normalize, uniq} = require('./lib')

test('normalize removes trailing and preceding slashes', () =>
  expect(normalize('/foo/')).toEqual('foo')
)
test('normalize removes query strings and fragment hashes from urls', () =>
  expect(normalize('http://foo.bar/baz?quux')).toEqual('http://foo.bar/baz')
)

test('uniq removes duplicates', () =>
  jsc.assert(jsc.forall('array nat', numbers => {
    const uniqueNumbers = uniq(numbers)
    return uniqueNumbers.every(number =>
      uniqueNumbers.filter(x => x === number).length === 1
    )
  }))
)
