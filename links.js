// @flow
const fetch = require('node-fetch')
const {JSDOM} = require('jsdom')

const {normalize, uniq} = require('./lib')

// $: apply a DOM (css) selector to some html string and return an array of
// matching elements
const $ = (selector/*: string */, html/*: string */)/*: Array<HTMLElement> */=>
  Array.from((new JSDOM(html)).window.document.querySelectorAll(selector))

// isRelative: a url is relative if starts with a slash but not two (like a
// protocol agnostic link)
const isRelative = (url/*: string */)/*: bool */=> /^\/[^/]/.test(url)

// fetchLinks: download some html from the specified url and return the
// links from the same domain as the specified baseUrl
const fetchLinks = (baseUrl/*: string */) =>
  async (url/*: string */)/*: Promise<Array<string>> */=> {
    const response = await fetch(url)
    const html = await response.text()
    const baseUrlStart = new RegExp(`^${baseUrl}`)

    const links = $('a[href]', html)
      // .reduce performs better than .map(getAttribute).filter(isRelative)
      .reduce((found, link) => {
        // it's ok to typecast to string, the selector ensures results have href
        const href = ((link.getAttribute('href')/*: any */)/*: string */)
          .replace(baseUrlStart, '')
        if (isRelative(href)) {
          // format relative links
          return [...found, `${baseUrl}/${normalize(href)}`]
        }
        return found
      }, [])

    return uniq(links)
  }

module.exports = {
  fetch: fetchLinks,
  $,
  isRelative
}
