# DEVELOPERS
This file describes some design considerations to keep in mind while developing
this tool, as well as other implementation details that may be important to know.

## Graphs and Trees
While a tree could be directly used during the crawling process, a graph takes
its place instead, since it better reflects the underlying structure of links
on a website. This allows further development of more features, for instance:

- knowing which url is the most linked from within the site
- knowing which page has the most / fewest links
- obtainig stats about the link structure (average links per page, etc.)

The tree is however obtained afterwards by parsing the entire links list, since
its the desired output for the cli tool.

## Algorithms
The graph exploration has been tweaked to allow for an asynchronous visitor
function (the one in charge of producing new graph nodes). In this tool it means
fetching and parsing the HTML for each link. The parsing itself is done using
`jsdom`.

The depth first exploration of the tree is enough for its current purposes of
displaying the target website's link structure, but not for a search, since the
visitor output is discarded and not taken into account for halting.

## CLI
The cli is mainly described using the `yargs` library. As of the first version,
it only provides means to show the url structure, but it could be easily
refactored into its own subcommand to enable more features. See the
[yargs docs](http://yargs.js.org/docs/) for details.

## Libraries
While external libraries have been kept to a minimum, utilities such as map,
reduce, uniq, etc., should come from trusted and tested implementations like
`ramda` or `lodash`. Since some algorithms have been written from scratch in this
codebase, it also includes the corresponding tests to provide some guarantees.

## Development Tools
Code follows the `standard` coding convention. Static typings via `flow` ensure
some type safety. Use `yarn lint` to check for both.

The main test tool is `jest`, while `jsverify` is only used to check for
correctness in the `uniq` algorithm implementation (although is pretty trivial).
Use `yarn test` to run all tests.

If this were a collaborative project, `husky` could be applied to git hooks to
ensure tests and linters are run prior to any push.
